# Cookbook
En principio será una recopilación de pequeños fragmentos de código en forma de chuleta rapida para consulta rapida.

<span style="color: red;">Cualquier persona que desee puede colaborar.</span>

## Código de ejemplos de Odoo

### Vistas

[Ocultar Campo de la vista](contenido/ocultar_campo_de_vista.md)

[Agregar css](contenido/agregar_css.md)

### Modelos

[Crear Campos Monetary](contenido/crear_campos_monetary.md)

[Cargar Campoo por Default](contenido/cargar_campoo_por_default.md)

[Descargar Documento](contenido/descargar_documento.md)

[Impresion Documentos](contenido/impresion_documentos.md)

[Leer Archivados](contenido/leer_archivados.md)

[Leer Directamente de la Base de Datos](contenido/leer_directamente_de_la_base_de_datos.md)

[Fusionar Pdf's](contenido/fusionar_pdf.md)

### Codigos

[Enviar email plantilla](contenido/enviar_email_plantilla.md)





