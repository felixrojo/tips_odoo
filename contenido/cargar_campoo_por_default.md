## Cargar un campo por defecto al inicio
        def _default_capte_fxd_file(self):
        file_path = 'C:\\borrar\\CAPTE.FXD'  # Ruta del archivo en la carpeta temp
        full_path = os.path.join(file_path)

        if os.path.exists(full_path):
            with open(full_path, 'rb') as file:
                return base64.b64encode(file.read())

    capte_fxd_file = fields.Binary(string='CAPTE.FXD File', default=_default_capte_fxd_file, filename="CAPTE.FXD", widget="binary")

[inicio](../readme.md)