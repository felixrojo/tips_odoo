## Crear campos monetary
    currency_id = fields.Many2one('res.currency', string='Moneda', required=True,
                                  default=lambda self: self.env.company.currency_id)
    invoice_amount_untax_signed = fields.Monetary(related="list_account_move_id.amount_untaxed_signed")

[inicio](../readme.md)