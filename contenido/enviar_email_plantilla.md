## Enviar email con plantilla.

         def action_enviar_email(self):
        self.ensure_one()

        # Configurar la plantilla de correo y enviar el correo
        # template_id = self.env.ref('mail.template_data_notification_email')
        template_id = self._find_mail_template()
        lang = self.env.context.get('lang')
        template = self.env['mail.template'].browse(template_id)

        if template:
            ctx = {
                # 'default_model': 'sale.order',
                # 'default_res_id': self.id,
                'default_use_template': bool(template.id),
                'default_template_id': template.id,
                'default_composition_mode': 'comment',
                'mark_so_as_sent': True,
            }

            mail_template = template
            mail_template.send_mail(self.id, force_send=True)

[inicio](../readme.md)