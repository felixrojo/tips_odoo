## Fusionar pdf y descargralo

**Necesita la biblioteca pymupdf**

    def printer_fusion_pdf(self, report):
        for doc in self:
            id = self.env['sale.order'].search([('name', '=', doc.origin)]).id
            records = self.env['sale.file.store'].search([('sale_id', '=', id), ('file_target', '=', 'supplier')])
            attachment_ids_start = records.file_ids
            self._create_temp_dir
            directory = ''
            if platform.system() == 'Linux':
                directory = '/tmp/'
            if platform.system() == 'Windows':
                directory = 'C:\\borrar\\'
            # record = self.env['sale.file.store'].browse(id)
            attachment_ids = []
            attachment_image_ids = []
            for record in attachment_ids_start:
                if record.mimetype == 'application/pdf':
                    attachment_ids.append(record)
                elif record.mimetype == 'image/png' or record.mimetype == 'image/jpeg' :
                    attachment_image_ids.append(record)
            if report == 'not_valuation':
                content, content_type = self.env.ref('custom_document_layout.action_report_purchase_order')._render_qweb_pdf(doc.id)
            else:
                content, content_type = self.env.ref(
                    'purchase.action_report_purchase_order')._render_qweb_pdf(doc.id)
            attachment = self.env['ir.attachment'].create({
                'name': 'generated_pdf.pdf',
                'type': 'binary',
                'datas': base64.b64encode(content),
                'res_model': 'sale.file.store',
                'res_id': doc.id
            })
            try:
                if doc_a:
                    doc_b = fitz.open(stream=attachment.raw, filetype="pdf")  # open the 2st document
                    doc_a.insert_pdf(doc_b)  # merge the docs
            except:
                doc_a = fitz.open(stream=attachment.raw, filetype="pdf")  # open the 1st document

            attachment.unlink()
            for record in attachment_ids:
                doc_b = fitz.open(stream=record.raw, filetype="pdf")  # open the 2nd document
                doc_a.insert_pdf(doc_b)  # merge the docs
            for record in attachment_image_ids:
                type_doc =record.mimetype.split('/')[1]
                doc_b = fitz.open(stream=record.raw, filetype=type_doc)  # open the 2nd document
                doc_a.insert_file(doc_b)  # merge the docs

        name_file = self._generate_name()
        doc_a.save(f"{directory}{name_file}.pdf")  # save the merged document with a new filename
        name_pdf = ''
        if len(self) == 1:
            name_pdf=self.name
        else:
            name_pdf = "Pedido de compra"
        merged_attachment = self.env['ir.attachment'].create({
            'name': f'{name_pdf.replace("."," ")}.pdf',
            'raw': open(f'{directory}{name_file}.pdf', 'rb').read(),
            'res_model': 'sale.file.store',
            'res_id': 9999999,
        })

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/content/%s?download=true' % merged_attachment.id,
            'target': 'new',
        }

[inicio](../readme.md)