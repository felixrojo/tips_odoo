## Leer directamente en la base de datos en vez de hacer un filtered

     self.env.cr.execute(
            f"""SELECT id
                FROM res_partner
                WHERE (LEFT(phone, 1) != '9' OR phone NOT LIKE '%/9%')
                AND LENGTH(phone) > 9
                AND phone <> '';""")
        ids = [i[0] for i in self.env.cr.fetchall()]
        data_valid = self.env['res.partner'].browse(ids)

[inicio](../readme.md)