## Que se pueda ocultar un campo en la vista
    <field name="export_dimoni" readonly="1" string="Diomoni" optional="show"/>

show y hide permiten que el campo se pueda esconder o mostrar en la vista. 

[inicio](../readme.md)